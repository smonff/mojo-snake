import $ from 'jquery'
import Mousetrap from 'mousetrap'
import { setDirection } from './snake'

const MOVING_TIME = 1

// Usage: logKey( e.key )
const logKey = ( key ) => {
  console.log( key + ' pressed')
}

/**
 * Unused...
 * The snake emoji direction is setted by classes
 */
// Was jQuery.fn...
$.fn.clearSnakeDirection = function( orientation ) {
  toClearClasses = $(this)[0].className.match( /direction--.{1}/g )
  $(toClearClasses).each( function(i, e) {
    $('.snake').removeClass(e)
  })
  $('.snake').addClass('direction--' + orientation)
}

const initKeybindings = () => {
  // Going west
  Mousetrap.bind('left', (e) => {
    $('.snake').animate({ left: '-=' + $('.snake').width() }, MOVING_TIME, function() {
      setDirection('W')
      const currentPositionX = $('.snake').data('x-position')
      $('.snake').data('x-position', Number(currentPositionX) - 1)
    })
  })

  // Going north
  Mousetrap.bind('up', (e) => {
    $('.snake').animate({ top: '-=' + $('.snake').height() }, MOVING_TIME, function() {
      setDirection('N')
      const currentPositionY = $('.snake').data('y-position')
      $('.snake').data('y-position', Number(currentPositionY) - 1)
    })
  })

  // Going east
  Mousetrap.bind('right', (e) => {
    $('.snake').animate({ left: '+=' + $('.snake').width() }, MOVING_TIME, function() {
      setDirection('E')
      const currentPositionX = $('.snake').data('x-position')
      $('.snake').data('x-position', Number(currentPositionX) + 1)
    })
  })

  // Going south
  Mousetrap.bind('down', (e) => {
    $('.snake').animate({ top: '+=' + $('.snake').height() }, MOVING_TIME, function() {
      setDirection('S')
      const currentPositionY = $('.snake').data('y-position')
      $('.snake').data('y-position', Number(currentPositionY) + 1)
    })
  })
}

export {
  initKeybindings
}
