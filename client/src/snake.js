import $ from 'jquery'

/**
 * TODO Once the snake is instanciated, it's size should be stored, it's a
 * constant
 */

const getHead = ( direction ) => {
  let head = 'E'
  switch( direction ) {
  case 'N':
    head = '&#9651;'
    break
  case 'E':
    head = '&#9655;'
    break
  case 'S':
    head = '&#9661;'
    break
  case 'W':
    head = '&#9665;'
    break
  }
  return head
}

const getSnake = ( direction ) => {
  return getHead( direction )
  // let tailLenght = 1
  // let tail = '='.repeat(tailLenght)
  // const snake = head + tail
  // return snake
}


const getDirection = () => {
  return $('.snake').data('direction')
}

const moveTo = ( x, y ) => {
  $('.snake').data('x-position', x)
  $('.snake').data('y-position', y)

  const xMove = $('.snake').width() * x
  const yMove = $('.snake').height() * y

  $('.snake').animate({
    left: '+=' + xMove,
    top: '+=' + yMove
  }, 1, function() {})
}


const setPosition = () => {

  const position = $('.snake').position()
  $('.debug .position').html( position.left + ' ' + position.top )

  // TODO Need a move(direction) function for mutualisation with actions in
  // keyboard.js - direction always available in .data('direction') -
  // Looks like browserling would be a very very good idea...

  const direction = $('.snake').data('direction')

  if ( direction === 'N' || direction === 'S' ) {
    const currentPositionY = $('.snake').data('y-position')
    if ( direction === 'S' ) {
      $('.snake').data('y-position', Number(currentPositionY) + 1)
      $('.snake').animate({ top: '+=' + $('.snake').height() }, 1, function() {})
    } else if ( direction === 'N' ) {
      $('.snake').data('y-position', Number(currentPositionY) - 1)
      $('.snake').animate({ top: '-=' + $('.snake').height() }, 1, function() {})
    }
  } else if ( direction === 'E' || direction === 'W' ) {
    const currentPositionX = $('.snake').data('x-position')
    if ( direction === 'E' ) {
      $('.snake').data('x-position', Number(currentPositionX) + 1)
      $('.snake').animate({ left: '+=' + $('.snake').width() }, 1, function() {})
    } else if ( direction === 'W' ) {
      $('.snake').data('x-position', Number(currentPositionX) - 1)
      $('.snake').animate({ left: '-=' + $('.snake').width() }, 1, function() {})
    }
  }
}

const setDirection = ( cardinality ) => {
  // TODO cardinality must be a general constant see #14
  $('.snake').data('direction', cardinality )
}

export {
  getDirection,
  getHead,
  getSnake,
  moveTo,
  setDirection,
  setPosition,
}
