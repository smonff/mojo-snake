import $ from 'jquery'
import { getDirection, getHead, getSnake, moveTo, setPosition, Snake } from './snake'
import { initKeybindings } from './keyboard'

$(() => {

  // Setting up a kind of canvas
  $('body').height(($(window).width() /2 ) - 10)
  $('.debug .width').html($('body').width())
  $('.debug .height').html($('body').height())

  var ws = new WebSocket('ws://localhost:3000/echo')

  /**
   * Note that if you want to play with some of the real-time snake parameters,
   * it should be done in the loop here
   */
  // Incoming messages
  ws.onmessage = function (event) {

    $('.snake').html( '' )
    const data = JSON.parse( event.data )
    $('.snake').html( data.snake )

    $('.debug .socket').html( data.ping )
    $('.debug .direction').html( data.direction )
    $('.debug .snake-head').html( $('.snake').width() + ' x ' + $('.snake').height() )
    $('.debug .fake-grid-width').html( Math.round( $('body').width() / $('.snake').width() ))
    $('.debug .fake-grid-height').html( Math.round( $('body').height() / $('.snake').height() ))
    $('.debug .fake-grid-x').html( $('.snake').data('x-position'))
    $('.debug .fake-grid-y').html( $('.snake').data('y-position'))

    if ( $('.snake').hasClass('new')) {
      moveTo( 45, 40 )
      $('.snake').removeClass('new')
    }
  }

  // Outgoing messages
  ws.onopen = function (event) {
    window.setInterval( function () {
      const direction = getDirection()
      ws.send(
        JSON.stringify({
          position: {
            x: $('.snake').data('x-position'),
            y: $('.snake').data('y-position')
          },
          snake: getSnake( direction ),
          direction: direction
        })
      )
    }, 250)
  }


  // From position.html.ep

  $('.snake').data('x-position', '0')
  $('.snake').data('y-position', '0')

  // $('.snake').animate({ top: '50%', left: '50%' })
  $('.snake').data('direction', 'E')


  setInterval( setPosition , 88 )

  initKeybindings()
})
