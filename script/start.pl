#!/usr/bin/env perl

use strict;
use warnings;

use lib qw(lib);
use Mojolicious::Commands;

# Start command line interface for application
Mojolicious::Commands->start_app('MojoSnake');
