package MojoSnake;
use Mojo::Base 'Mojolicious';
use Data::Printer;
use Mojo::JSON qw< encode_json decode_json >;

sub startup {
  my $self = shift;

  my $r = $self->routes;

  $r->get( '/' => 'index');

  # WebSocket echo service
  $r->websocket(
    '/echo' => sub {
      my $c = shift;
      my $i = 0;
      # Opened
      $c->app->log->debug('WebSocket opened');

      # Increase inactivity timeout for connection a bit
      $c->inactivity_timeout(300);

      # Incoming message
      $c->on(
        message => sub {
          my ($c, $msg) = @_;
          my $JSON_msg = decode_json $msg;
          #p $JSON_msg;
          $c->app->log->info(
            "WebSocket $i " .
              "move " .
              $JSON_msg->{ direction } .
              " (" .
              $JSON_msg->{ position }->{ x } .
              ", " .
              $JSON_msg->{ position }->{ y }  .
              ")"
             );
          $c->send(
            encode_json({
              direction => $JSON_msg->{direction},
              ping => $i,
              snake => $JSON_msg->{snake},
            }));
          $i++;
        });

      # Closed
      $c->on(
        finish => sub {
          my ($c, $code, $reason) = @_;
          $c->app->log->debug("WebSocket closed with status $code");
        });
    });
};

1;
